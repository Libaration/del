var Recent = () => {
  const [recentAuctions, setRecentAuctions] = React.useState(null);
  React.useEffect(() => {
    const fetchAndSetRecent = async () => {
      const recent = await fetchRecent();
      setRecentAuctions(recent);
    };
    fetchAndSetRecent();
  }, []);
  const renderRecent = () => {
    if (recentAuctions !== null) {
      return recentAuctions.map((auction) => (
        <Auction key={auction.auction_id} auction={auction} />
      ));
    } else {
      return (
        <div className="text-center">
          <span className="text-lg">Loading....</span>
        </div>
      );
    }
  };
  return (
    <React.Fragment>
      <div className="flex wrap mb-5">{renderRecent()}</div>
    </React.Fragment>
  );
};
