const domContainer = document.querySelector("#divContent");
var root = ReactDOM.createRoot(domContainer);

const Index = () => {
  const textRef = React.useRef(null);
  const [title, setTitle] = React.useState(null);
  React.useEffect(() => {
    setTitle(document.title);
  }, [document, textRef]);
  return (
    <React.Fragment>
      <div className="font-sans">
        <h1
          ref={textRef}
          className="font-extrabold text-transparent text-7xl bg-clip-text bg-gradient-to-r from-blue-900 to-cyan-900 text-center pt-5 pb-5"
        >
          {title}
        </h1>
      </div>
      <Recent />
      <ListingForm />
    </React.Fragment>
  );
};
root.render(<Index />);
