var Auction = (props) => {
  const { auction } = props;
  const image = auction.highlights[0].cached_assets[0].url;
  return (
    <div className="max-w-sm rounded overflow-hidden shadow-lg">
      <img className="w-full" src={image} />
    </div>
  );
};
